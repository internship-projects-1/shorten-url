import User from "../models/user.js";
import { setUser } from "../service/auth.js";

const handleUserSignUp = async (req, res) => {
  const { username, email, password } = req.body;

  const createdUser = await User.create({
    username: username,
    email: email,
    password: password,
  });

  if (createdUser === null || createdUser === undefined) {
    return res.render("signup", {
      error: "Could not create account",
      baseUrl: process.env.BASE_URL,
    });
  }

  return res.redirect("/home");
};

const handleUserLogin = async (req, res) => {
  const { email, password } = req.body;

  const user = await User.find({ email, password });

  if (user.length === 0) {
    return res.render("login", {
      error: "Invalid email or password",
      baseUrl: process.env.BASE_URL,
    });
  }

  const token = setUser(user);
  res.cookie("uid", token);
  return res.redirect("/home");
};

export { handleUserSignUp, handleUserLogin };
