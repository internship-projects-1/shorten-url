import { nanoid } from "nanoid";
import URL from "../models/url.js";

const hanldeGenerateNewShortURL = async (req, res) => {
  const { url } = req.body;

  if (!url) {
    return res.status(400).json({ error: "url is required" });
  }

  let shortID = nanoid();
  shortID = shortID.substring(1, 9);

  const createdUrl = await URL.create({
    shortID: shortID,
    redirectURL: url,
    visitHistory: [],
  });

  if(createdUrl) {
    return res.status(201).render("home", {
      id: shortID,
      baseUrl: process.env.BASE_URL
    });
  }

  return res.redirect("/home");

};

const handleVisitShortURL = async (req, res) => {
  const shortID = req.params.shortID;

  const fetchedData = await URL.findOneAndUpdate(
    { shortID },
    {
      $push: {
        visitHistory: {
          timestamp: Date.now(),
        },
      },
    }
  );

  return res.status(200).redirect(fetchedData?.redirectURL);
};

const handleGetAnalytics = async (req, res) => {
  const shortID = req.params.shortID;

  const data = await URL.findOne({ shortID });

  return res.status(200).json({
    totalClick: data.visitHistory.length,
    analytics: data.visitHistory,
  });
};

const handleDeleteUrl = async (req, res) => {
  const { button } = req.body;

  await URL.deleteOne({ shortID: button });

  return res.redirect("/home");
};

export {
  hanldeGenerateNewShortURL,
  handleVisitShortURL,
  handleGetAnalytics,
  handleDeleteUrl,
};
