import { getUser } from "../service/auth.js";

async function restrictToLoggedInUserOnly(req, res, next) {
  const token = req.cookies?.uid;

  if (!token) return res.redirect("/login");
  const user = getUser(token);

  if (!user) return res.redirect("/login");

  req.user = user;
  next();
}

export default restrictToLoggedInUserOnly;
