import express from "express";
import { handleGetAnalytics, handleVisitShortURL, hanldeGenerateNewShortURL, handleDeleteUrl } from "../controllers/url.js";

const router = express.Router();

router.post("/", hanldeGenerateNewShortURL);
router.get("/:shortID", handleVisitShortURL);
router.get("/analytics/:shortID", handleGetAnalytics);
router.post("/delete", handleDeleteUrl);

export default router;