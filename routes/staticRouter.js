// This file contains all the routes where we need to send html as response from the server

import express from "express";

const router = express.Router();

router.get("/", async (req, res) => {
  return res.render("landing", {
    baseUrl: process.env.BASE_URL,
  });
});

router.get("/home", async (req, res) => {
  const allUrls = await URL.find({});
  return res.render("home", {
    urls: allUrls,
    baseUrl: process.env.BASE_URL,
  });
});

router.get("/login", (req, res) => {
  return res.render("login", {
    baseUrl: process.env.BASE_URL,
  });
});

router.get("/signup", (req, res) => {
  return res.render("signUp", {
    baseUrl: process.env.BASE_URL,
  });
});

export default router;
