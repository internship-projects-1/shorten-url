import express from "express";
import { handleUserSignUp, handleUserLogin } from "../controllers/user.js";

const router = express.Router();

router.post("/login", handleUserLogin);
router.post("/", handleUserSignUp);

export default router;
