// import express from "express";
import express from "express";
import urlRoute from "./routes/url.js";
import staticRoute from "./routes/staticRouter.js";
import userRoute from "./routes/user.js";
import connectToMongoDB from "./connect.js";
import path from "path";
import { fileURLToPath } from "url";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";

const app = express();
const PORT = process.env.PORT || 3000;
dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

app.set("view engine", "ejs");
app.set("views", path.resolve("./views"));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

connectToMongoDB(
  `mongodb+srv://mukulpadwal:${process.env.MONGO_ATLAS_PASSWORD}@cluster0.dzs2chr.mongodb.net/short-url?retryWrites=true&w=majority`
).then(() => {
  console.log("Connection to database is successfull!!!");
});

app.use("/", staticRoute);
app.use("/url", urlRoute);
app.use("/user", userRoute);

app.listen(PORT, () => {
  console.log(`Server is up and running!!!`);
});
