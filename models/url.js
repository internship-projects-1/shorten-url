import mongoose from "mongoose";

const urlSchema = mongoose.Schema(
  {
    shortID: {
      type: String,
      required: true,
    },
    redirectURL: {
      type: String,
      required: true,
    },
    visitHistory: [
      {
        timestamp: {
          type: Number,
        },
      },
    ],
  },
  { timestamps: true }
);

export default URL = mongoose.model("url", urlSchema);
